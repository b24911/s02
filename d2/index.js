//alert("Hello B249!")

// create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];


function login(email) {
	console.log(`${email} has logged in`);
}

login(studentOneEmail);

function logout(email) {
	console.log(`${email} has logged out`);
}

function listGrades(grades){
	grades.forEach(grade => {
		console.log(grade)
	})
}
//spaghetti code - code that is so poorly organized that it becomes impossible to work with

//use an object literal: {}
//ENCAPSULATION - the organization of information (properties) and behavior(as methods) to belong to the object that encapsulates them (the scope of encapsulation is denoted by the object literals)
let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],


//add the functionalities available to a student as object methods
		//the keyword "this" refers to the object encapsulating the method where "this" is called

	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		//console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		console.log(`${this.name}'s grades are :`, this.grades);
	},
	//Mini-exercise 1
	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/4;

 	// const total = this.grades.reduce((a, b) => a + b, 0);
  	// return total / this.grades.length;
	},

	// computeAve(){
	// 	return this.grades.reduce((a,b) => {
	// 		return a+b
	// 	})/(this.grades.length);
	// }
	//computeAve(){
	//let sum = 0;
	//this.grades.forEach(grade => sum = sum + grade);
	//return sum/4;
	//}

	//Mini-exercise 2

	willPass(){
	//		return this.computeAve() >= 85 ? true : false
		//condition ? value if condition is true : value if condition is false

		if(this.computeAve() >= 85){
			return true;
		}else {
			return false;
		}
		//hint you can call methods inside an object
	},

	// Mini-exercise 

	willPassWithHonors(){
		//return (this.willPass() && this.computeAve() >= 90) ? true : false;
	
		// if(this.computeAve() >= 90 && this.willPass() === true){
		// 	return true;
		// }else {
		// 	return false;
		// }
		if(this.computeAve() >= 90 && this.willPass() === true){
			return true;
		if(this.computeAve() < 85)
			return undefined;
		}
	
	}
}
//console.log(`${this.name}'s grades are :`, this.grades);
console.log(studentOne.computeAve());
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's grades is ${studentOne.grades}`);

// ACTIVITY

/*
1.Spaghetti code
2.with the use of curly braces{}
3.Encapsulation
4.studentOne.enroll()
5.true
6.dot notation
7.true
8.true
9.true
10.true

*/

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s grades are :`, this.grades);
	},
	computeAve(){
 	const total = this.grades.reduce((a, b) => a + b, 0);
  	return total / this.grades.length;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		if(this.computeAve() >= 90 && this.willPass() === true){
			return true;
		if(this.computeAve() < 85)
			return undefined;
		}
	}
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s grades are :`, this.grades);
	},
	computeAve(){
 	const total = this.grades.reduce((a, b) => a + b, 0);
  	return total / this.grades.length;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		if(this.computeAve() >= 90 && this.willPass() === true){
			return true;
		if(this.computeAve() < 85)
			return undefined;
		}
	}
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s grades are :`, this.grades);
	},
	computeAve(){
 	const total = this.grades.reduce((a, b) => a + b, 0);
  	return total / this.grades.length;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		if(this.computeAve() >= 90 && this.willPass() === true){
			return true;
		if(this.computeAve() < 85)
			return undefined;
		}
	}
}


let classOf1A = {
	students: [studentOne,studentTwo,studentThree,studentFour],

	countHonorStudents(){
		honors = []
		this.students.forEach((stud)=> {
			if(stud.willPassWithHonors()){
				honors.push(stud)
			}
		})
		return honors.length;
	},

	honorsPercentage(){
		let percent = (100 / this.countHonorStudents()) ;
		//return (percent + '%');
		return `${percent}%`
	},

	retrieveHonorStudentInfo(){
		honors = []
		aveGrade 	}
}
